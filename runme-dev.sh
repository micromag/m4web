#!/bin/bash

PORT=${1:-${PYRAMID_PORT:?}}

# python3 setup.py egg_info

gunicorn --reload --bind :${PORT} --paste development.ini
