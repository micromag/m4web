from setuptools import setup, find_packages

requires = [
    'requests',
    'gunicorn',
    'jquery_alchemy',
    'pyramid',
    'pyramid_chameleon',
    'pyramid_debugtoolbar',
    'pyramid_beaker',
    'pyramid_tm',
    'cx_oracle',
    'SQLAlchemy',
    'transaction',
    'wtforms',
    'wtforms_alchemy',
    'zope.sqlalchemy',
    'python-dateutil',
    'bokeh'
]

setup(name='m4web',
      version='0.1',
      description='m4web application',
      packages = find_packages(),
      package_data={'m4web':['static/*','templates/*']},
      data_files=[('/etc/m4web/',['production.ini', 'm4web.ini'])],
      entry_points="""\
      [paste.app_factory]
      main = m4web:main
      [console_scripts]
      initialize_m4web_db = m4web.initialize_db:main
      """,
)
