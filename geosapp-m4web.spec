%global appname m4web

Name:		geosapp-%{appname}
Version:	0.1
Release:	1.geos
Summary:	School of GeoSciences m4web Application

License:	BSD
URL:		www.geos.ed.ac.uk
Source0:	%{appname}-%{version}.tar.gz


BuildArch:      noarch

BuildRequires:	python36-devel
BuildRequires:  python36-setuptools

Requires:	python36-pyramid
Requires:	python36-gunicorn
Requires:	python36-requests
Requires:	python36-jquery_alchemy
Requires:	python36-pyramid-chameleon
Requires:	python36-pyramid-debugtoolbar
Requires:	python36-pyramid-beaker
Requires:	python36-pyramid-tm
Requires:	python36-psycopg2
Requires:	python36-zope-sqlalchemy
Requires:	python36-wtforms
Requires:	python36-wtforms-components
Requires:	python36-dateutil
Requires:	python36-wtforms-alchemy
Requires:	python36-validators
Requires:	python36-decorator
Requires:	python36-intervals
Requires:	python36-infinity
Requires:	python36-sqlalchemy-utils
Requires:	python36-bokeh

%description
School of GeoSciences m4web application

An application to display m4web model data

%prep
%setup -q -n %{appname}-%{version}
# Remove bundled egg info
rm -rf %{modname}.egg-info

%build
%{__python3} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python3} setup.py install --skip-build --root=%{buildroot}



%files
%doc
%config %{_sysconfdir}/%{appname}/%{appname}.ini
%config %{_sysconfdir}/%{appname}/production.ini
%{python3_sitelib}/%{appname}/
%{python3_sitelib}/%{appname}-%{version}*.egg-info
%{_bindir}/initialize_%{appname}_db

%changelog
* Fri Dec 21 20XX Chris Hill <C.Hill@ed.ac.uk> - 0.0-1.geos
- Initial build
