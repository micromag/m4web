#!/bin/bash

PKGNAME=geosapp-$(python3 setup.py --name)

setupver=$(python3 setup.py --version)
rpmver=$(rpm -q --specfile $PKGNAME.spec --queryformat '%{VERSION}\n' | uniq)
if [ "${setupver}" != "${rpmver}" ] ; then
    echo "Version in setup.py (${setupver}) different to spec file (${rpmver})"
    exit 1
fi

vers=$(rpm -q --specfile $PKGNAME.spec --queryformat '%{VERSION}-%{RELEASE}\n' | uniq)
python3 setup.py sdist
rpmbuild --define "_sourcedir dist" --define "_srcrpmdir ." -bs $PKGNAME.spec
rpmbuild --rebuild $PKGNAME-$vers.src.rpm
