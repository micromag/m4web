import pyramid.view
import copy

from . import geb
from . import models

geos_view = geb.geb_view(
    menu_items=[
        # Menu title, route name, permissions required to view
        # Permissions are set in Class Root at bottom of models.py
        ("Model search", "models_report_view", "view"),
    ],
    title="m4web",
    edgel_colour="blue-bright",
)


@pyramid.view.view_config(route_name="home", renderer="templates/geb.pt")
@geb.RequireEase
@geos_view
def root_view(request):
    return {
        "banner": request.static_url("static/EdWebBannerImage.jpg"),
    }


@pyramid.view.notfound_view_config(
    renderer="templates/geb.pt", append_slash=True)
@geos_view
def exception_view(request):
    return {
        "banner": request.static_url("static/EdWebBannerImage.jpg"),
        "exception": request.exception,
        "url": request.route_url("home"),
    }


@pyramid.view.forbidden_view_config(renderer="templates/geb.pt")
@geb.RequireEase
def forbidden_view(request):
    return exception_view(request)


@pyramid.view.view_config(
    route_name="models_report_view", renderer="templates/geb.pt")
@geb.RequireEase
@geos_view
def models_report_view(request):
    """ Form to query database """

    def create_form(request=None):
        if request:
            form_models = models.ModelsSearchForm(request.POST)
        else:
            form_models = models.ModelsSearchForm(formdata=None)
        form_models.material_name.choices = models.Material_.get_choices()
        form_models.geometry_name.choices = models.Geometry_.get_choices()
        return form_models

    # Set maximum number of rows to display on page
    display_limit = 500
    panel_table = geb.DisplayPanel()
    form_models = create_form(request)
    panel_form = geb.DisplayPanel()
    panel_form.append(form_models)
    if request.POST:
        if form_models.reset.data:
            # Overwrite panel_form and form_models with form with no data
            # https://stackoverflow.com/questions/26764197/how-to-empty-a-numeric-value-from-a-form-built-with-wtform
            form_models = create_form()
            panel_form = geb.DisplayPanel()
            panel_form.append(form_models)
        if form_models.submit.data:
            panel_table.append(geb.DisplayHeading("Models report", "h2"))
            # Create dictionary of user choices
            choices_dict = copy.deepcopy(form_models.data)
            for key in ["geb_confirm", "submit", "reset"]:
                del choices_dict[key]
            print(
                "form_models.geometry_size_minimum.data",
                form_models.geometry_size_minimum.data,
            )
            form_models.geometry_size_minimum.data = choices_dict[
                "geometry_size_minimum"
            ]
            print(
                "form_models.geometry_size_minimum.data",
                form_models.geometry_size_minimum.data,
            )
            # Check for missing inputs and alert user
            missing_data = False
            for key, value in choices_dict.items():
                if value == "" or not value:
                    request.session.flash(
                        "Please select a valid value for " + key.replace(
                            "_", " "),
                        "warning",
                    )
                    missing_data = True

            if not missing_data:
                table, row_count = models_report(
                    request, choices_dict, display_limit)
                if row_count > display_limit:
                    print("row_count > display_limit", row_count)
                    feedback_string = "{} models returned. Only the first {} are displayed.".format(
                        format_int(row_count), display_limit
                    )
                    request.session.flash(feedback_string, "info")
                else:

                    feedback_string = "{} models returned".format(
                        format_int(row_count))
                    request.session.flash(feedback_string, "info")
                # debug
                panel_table.append(table)

    results = {
        # Invert comments to put form on right
        # 'main': panel_table,
        "main": geb.RenderList([panel_form, panel_table]),
        # 'right': panel_form,
        # 'flashes': []
    }
    return results


def models_report(request, choices_dict, display_limit):
    """ Create DisplayTable for models returned from search """
    models_list, row_count = models.Model_.search_parameters(
        choices_dict["material_name"],
        choices_dict["geometry_name"],
        choices_dict["geometry_size_minimum"],
        choices_dict["geometry_size_maximum"],
        choices_dict["material_temperature_minimum"],
        choices_dict["material_temperature_maximum"],
        display_limit,
    )

    table = geb.DisplayTable(
        headings=[
            "Geometry",
            "Size",
            "Material",
            "Temperature",
            "mx_tot",
            "my_tot",
            "mz_tot",
            "h_tot",
            "e_tot",
            " ",
        ],
        row_headings=False,
        align=["left", ],
        style="striped",
    )
    for model in models_list:
        model_row = list(model._asdict().values())[1:]
        widget = geb.LinkWidget(
            request.route_url("model_detail", id=model.id),
            "View",
            appearance="outline small",
            data_uoe_button="info-sign",
            data_uoe_button_type="btn-outline-uoe",
        )
        model_row.append(widget)
        table.append(model_row)
    return table, row_count


@pyramid.view.view_config(
    route_name="model_detail", renderer="templates/geb.pt")
@geb.RequireEase
@geos_view
def model_detail(request):
    """ Single model display page """
    id = request.matchdict.get("id", None)
    model_ = models.Model_.get_by_id(id=id)
    if id:
        try:
            model_ = models.Model_.get_by_id(id)
        except:
            model_ = None
        if not model_:
            raise pyramid.httpexceptions.HTTPNotFound()
    table = geb.DisplayTable(style="hover", row_headings=True)
    table.append(("Model unique_id", model_.unique_id))
    table.append(("Geometry name", model_.geometry_name))
    table.append(("Geometry size", model_.size_decimal))
    table.append(("Material name", model_.material_name))
    table.append(("Material temperature", model_.temperature_decimal))
    table.append(("Model mx_tot", model_.mx_tot))
    table.append(("Model my_tot", model_.my_tot))
    table.append(("Model mz_tot", model_.mz_tot))
    table.append(("Model h_tot", model_.h_tot))
    table.append(("Model e_tot", model_.e_tot))
    panel_table = geb.DisplayPanel(
        title=geb.DisplayHeading("Model %s" % model_.id))
    panel_table.append(table)
    return {
        "main": panel_table,
    }


def format_int(i):
    """ Format an integer as a string for display in a sentence """
    units = [
        "Zero",
        "One",
        "Two",
        "Three",
        "Four",
        "Five",
        "Six",
        "Seven",
        "Eight",
        "Nine",
    ]
    if i <= 9:
        i = units[i]
    return str(i)
