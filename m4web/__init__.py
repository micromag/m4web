import pyramid
import pyramid.config
import pyramid.session
import pyramid.authentication
import pyramid.authorization
import sqlalchemy

from . import geb
from . import models


def main(global_config, **settings):
    engine = sqlalchemy.engine_from_config(settings, 'sqlalchemy.')
    geb.DBSession.configure(bind=engine)
    geb.Base.metadata.bind = engine

    authn_policy = pyramid.authentication.RemoteUserAuthenticationPolicy(
        callback=models.ApplicationRoles.get_roles, debug=True)
    authz_policy = pyramid.authorization.ACLAuthorizationPolicy()

    config = pyramid.config.Configurator(settings=settings,
                                         root_factory='.models.Root')
    # config = pyramid.config.Configurator(settings=settings)
    config.set_authentication_policy(authn_policy)
    config.set_authorization_policy(authz_policy)
    config.include('pyramid_beaker')
    config.include('pyramid_chameleon')
    config.add_renderer('json', geb.JSONRenderer())
    config.add_renderer('xml', geb.XMLRenderer())
    config.add_static_view('static', 'static', cache_max_age=3600)
    # Routes: route name, url suffix

    # config.add_route('m4web_user_privileged', '/privileged')
    config.add_route('models_report_view', '/models-report')
    config.add_route('model_detail', '/model/{id}')
    config.add_route('home', '/')
    config.add_route('ajax', '/ajax/*ajax')

    config.scan()
    return geb.ProxyHeaders(config.make_wsgi_app(), settings)
