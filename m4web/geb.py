"""geb - GeoSciences and Engineering Base for Pyramid Framework apps.

   These will need to be updated periodically to keep things in step, but
   it is expected that there will be an element of drift in return for
   stability of deployed packages.
"""

import pyramid
import pyramid.renderers
import pyramid.httpexceptions
import wtforms
import wtforms_alchemy
import wtforms_components
import zope.sqlalchemy
import sqlalchemy
import sqlalchemy.orm
import sqlalchemy.exc
import sqlalchemy.ext.declarative
import collections
import datetime
import itertools
import xml.etree.ElementTree
import html
import decimal
import logging
import json
import io
import base64
import csv
import markupsafe
from PIL import Image


log = logging.getLogger(
    __name__[:-4] if __name__.endswith('.geb') else __name__)

DBSession = sqlalchemy.orm.scoped_session(sqlalchemy.orm.sessionmaker())
zope.sqlalchemy.register(DBSession)

class GetSession:
    @staticmethod
    def get_session():
        # this method should return sqlalchemy session
        return DBSession

class ProxyHeaders:
    '''Application wrapper to make use of headers sent from the proxy server,
    if one is being used.

    :param app: the WSGI application
    '''
    def __init__(self, app, settings=None):
        self.app = app
        self.settings = settings

    def __call__(self, environ, start_response):
        script_name = environ.get('HTTP_X_SCRIPT_NAME', '')
        remote_user = environ.get('HTTP_REMOTE_USER', '')
        scheme = environ.get('HTTP_X_SCHEME', '')
        uri = environ.get('HTTP_SCRIPT_URI', '')

        if script_name:
            environ['SCRIPT_NAME'] = script_name
            path_info = environ['PATH_INFO']
            if path_info.startswith(script_name):
                environ['PATH_INFO'] = path_info[len(script_name):]

        if scheme:
            environ['wsgi.url_scheme'] = scheme
        elif uri.startswith('http://'):
            environ['wsgi.url_scheme'] = 'http'
        elif uri.startswith('https://'):
            environ['wsgi.url_scheme'] = 'https'

        if not remote_user and self.settings:
            remote_user = self.settings.get('geb.fake_ease')

        if remote_user:
            environ['REMOTE_USER'] = remote_user

        return self.app(environ, start_response)

def geb_view(func=None, **kwargs):
    """
    Decorator to create standard view for user interaction.

    The standard view will perform form validation and database
    flushing where appropriate and handle error conditions.

    The wrapped function can return a pyramid HTTPException which is
    passed on unchanged.  Otherwise it should return a dict (or None)

    The returned dict is augmented with default values.
    Additional default values can be passed to the decorator as
    named arguments.

    The result dictionary can contain the following:

    * main: The main part of the web page.  Usually a panel
      default - DisplayPanel, containing  form or table
    * menu: Rendered as the left-hand menu
      default - None
    * menu_root: if menu not provided, use as title for root in menu
      default - 'School of GeoSciences home'
    * menu_items: if menu is not provided, use NavMenu with these items
      default - []
    * form: A form to be displayed and validated as appropriate
      default - None
    * obj: The object to be populated by  form
      default - None
    * route_call: if present, called with (request, form, obj)
      should return url to load after successful update.
    * url: if route_call not present, url to load after successful update.
      default - url is found with request.current_route_url()
    * valid_success: Flash message to display on successful update
      default - None
    * valid_warn: Flash message to display on failed validation
      default - None (field validation errors will be displayed)
    * flashes: list of possible flash quesues to display
      default - ['danger', 'warning', 'info', 'success']
    * title: Title for HTML in web page
      default - 'School of GeoSciences'
    * heading: Main heading to display at top of page
      default - title
    * heading_url: Destination for link from main heading
      default - '#' (i.e. the current page)
    * static: url for static items (used in template)
      default - request.static_url('static/')
    * banner: An image file for the header area
      default - None.  If no / in name, prepend static
    * edgel_colour: Theme colour from EdGel pallette
      default - None, equivalent to 'red'
    * edgel_background: Numerical colour for use in CSS
      default - #d50032 the red colour
    * edgel_color: Numerical color for use in CSS (cf. edgel_colour)
      default - #fff white to contrast with background
    * ajax: url for ajax call backs
      default - request.route_url('ajax', ajax=())
    """

    def _wrapper(function):
        def _view(request):
            fn_result = function(request)
            if isinstance(fn_result, pyramid.httpexceptions.HTTPException):
                raise fn_result

            result = {}
            result.update(kwargs)

            if fn_result:
                result.update(fn_result)

            if 'menu' not in result:
                result['menu'] = NavMenu(
                    request,
                    root=result.get('menu_root', 'School of GeoSciences home'),
                    *result.get('menu_items', []))
            result.setdefault('static', request.static_url('static/'))
            banner = result.get('banner')
            if banner and '/' not in banner:
                result['banner'] = result['static'] + banner
            result.setdefault('flashes',
                              ['danger', 'warning', 'info', 'success'])
            result.setdefault('title', 'School of GeoSciences')
            result.setdefault('heading', result['title'])
            if 'ajax' not in result:
                result['ajax'] = request.route_url('ajax', ajax=())

            if 'main' not in result:
                main_panel = DisplayPanel()
                if 'form' in result:
                    main_panel.append(result['form'])
                elif 'table' in result:
                    main_panel.append(result['table'])
                result['main'] = main_panel

            if 'form' in result:
                form = result['form']
                if request.method == 'POST':
                    result['geb_modified'] = 1
                    if form.validate():
                        try:
                            obj = result.get('obj')
                            if 'populate' in result:
                                result['populate'](form, obj)
                            elif obj is not None:
                                form.populate_obj(obj)

                            DBSession.flush()

                            if 'route_call' in result:
                                url = result['route_call'](request, obj, form)
                            else:
                                url = result.get('url',
                                                 request.current_route_url())
                            action = form.get_action()
                            if action is not None:
                                request.session.flash(action, 'action')
                                success = result.get('valid_success')
                                if success:
                                    request.session.flash(
                                        success, 'success', False)
                            if url != '.':
                                return pyramid.httpexceptions.HTTPFound(
                                    location=url)
                            result['geb_modified'] = 0
                        except (
                                sqlalchemy.exc.ResourceClosedError,
                                sqlalchemy.exc.SQLAlchemyError,
                        ) as err:
                            DBSession.rollback()
                            now = datetime.datetime.now().strftime(
                                "%Y%m%d-%H%M%S")
                            if in_development(request):
                                request.session.flash(
                                    "ERROR: %s" % err, 'warning', False)
                            else:
                                log.error("%s: %s" % (now, err))

                            request.session.flash(
                                'The database would not accept your input. '
                                'Please report this quoting %s' % now,
                                'danger', False)
                    else:
                        warning = result.get('valid_warn')
                        if warning:
                            request.session.flash(warning, 'warning', False)
            return result
        return _view

    if func:
        return _wrapper(func)
    return _wrapper


class ColumnChoices(GetSession):
    """A base class which is callable to generate choices from a column.

    The choices are pairs of value and display, the display column is optional.
    If omitted the value column is used.  Returns the list sorted and uniqued.

    When called takes an optional value.  If the value is provided, the
    returned "list" contains only the matching item.
    When called also takes an optional search - this is used to generate
    an ilike filter on the displayed values.

    This allows display of the current value when using ajax for the choices.
    """
    def __init__(self, column, display=None, search_in=None):
        self.column = column
        self.display = column if display is None else display
        self.search_in = self.display if search_in is None else search_in

    def __call__(self, value=None, search=None):
        query = self.get_session().query(self.column, self.display)
        if value:
            query = query.filter(self.column == value)
        if search:
            query = query.filter(self.search_in.ilike('%' + search + '%'))
        return query.order_by(self.display).distinct().all()

class HTMLString(markupsafe.Markup):
    pass

class AppearanceClass:

    @staticmethod
    def appearance_class(appearance=None):

        if not appearance:
            return ""

        buttons = {
            'button': ("btn-default", "small"),
            'warning': ("btn-warning", "small"),
            'delete': ("btn-danger", "small"),
            'error': ("btn-danger", "small"),
            'submit': ("btn-primary", "large"),
            'action': ("btn-default", "large"),
            'outline': ("btn-outline-uoe", "large"),
            'option': ("btn-primary", "medium"),
            'btn-uoe': ("btn-uoe", "medium"),
        }
        sizes = {
            "small": "btn-sm",
            "large": "btn-lg",
            "medium": "",
        }

        classes = ["btn"]
        implied_size = "medium"

        appearances = appearance.lower().split()

        for appnce, cls_size in buttons.items():
            if appnce in appearances:
                class_, size = cls_size
                classes.append(class_)
                implied_size = size

        for size, class_ in sizes.items():
            if size in appearances:
                classes.append(class_)
                break
        else:
            classes.append(sizes.get(implied_size, ""))
        return " ".join(classes)

class LinkWidget(HTMLString, AppearanceClass):
    """
    Makes an HTML link suitable for rendering in a template.

    If no text is provided, the link text is used.
    If a glyph is specified, the text is used to label the text.
    Other appearance options can be added, including 'button'.
    Optional confirm - see confirm_kwargs
    """
    def __new__(cls, link, text=None, appearance=None,
                glyph=None, confirm=None, **kwargs):
        if text is None:
            text = link
        if glyph:
            text = ('<button title="%s" aria-label="%s" '
                        'class="btn btn-link">'
                        '<span class="glyphicon glyphicon-%s" />'
                    '</button>' % (text, text, glyph))

        class_ = 'class="%s"' % cls.appearance_class(appearance)
        attrs = [class_]
        for key, value in kwargs.items():
            attrs.append('%s="%s"' % (key.replace('_', '-'), value))
        for key, value in confirm_kwargs(confirm).items():
            attrs.append('%s="%s"' % (key, value))
        self = super().__new__(cls, '<a %s href="%s">%s</a>' %
                               (' '.join(attrs), link, text))
        return self

class DisplayMugshot(HTMLString):

    def __new__(cls, url, width=80):
        return super().__new__(cls, '<div style="width:%dpx">'
                               '<img src="%s" /></div>' % (width, url))

class DisplayImage(HTMLString):
    # Image is full width of container (cf DisplayMugshot())
    def __new__(cls, url, vspace=0):
        return super().__new__(cls, '<div>'
            '<img src="%s" vspace="%d"/></div>' % (url, vspace))


class RenderHTML:
    """
    Class which adds a static method to render items.

    Items with an __html__ method return the result of that method.
    Other items return an html escaped version of their string value.
    None is a special case and returns an empty string.
    """
    @staticmethod
    def item_render(item):
        if hasattr(item, '__html__'):
            return item.__html__()
        elif item is not None:
            return html.escape(str(item))
        return ''

class RenderList(list, RenderHTML):
    """
    A list container which renders each of its items.

    This behaves as a python list in all respects, except when rendered
    the individual items are rendered, and concatentated together.
    See DisplayList if an HTML list is required.
    """
    def __html__(self):
        return '\n'.join(self.item_render(item) for item in self)

class HTMLDivClass(HTMLString):
    """
    Makes a set of nested divs with the relevant content - presumed safe.

    The divs provided have a list of strings which are the classes
    to be supplied to each nested div.
    Also accepts a set of keyword arguments to set attributes of
    the rendered divs.
    """

    def __new__(cls, *args, **kwargs):
        content = args[-1]
        attrs = []
        for attr, value in kwargs.items():
            attr = attr.rstrip('_').lower()
            if value is True:
                attrs.append(attr)
            elif value not in (False, None):
                attrs.append('%s="%s"' % (attr, value))
        attrs = ' '.join(attrs)
        for div_class in reversed(args[:-1]):
            content = '\n<div class="%s" %s>\n%s\n</div>\n' % (
                div_class, attrs, content)
        return super().__new__(cls, content)

class DivClass(RenderHTML):
    """
    Makes a set of nested divs with the relevant content (HTML escaped).

    The content is rendered or converted to HTML.
    The divs provided have a list of strings which are the classes
    to be supplied to each nested div.
    Also accepts a set of keyword arguments to set attributes of
    the rendered divs.
    """

    def __init__(self, *div_classes, **kwargs):
        self.content = []
        self.div_classes = div_classes
        self.kwargs = kwargs

    def append(self, *items):
        self.content += items
        return self

    def __html__(self):
        content = []
        for item in self.content:
            content.append(self.item_render(item))
        return HTMLDivClass(*(list(self.div_classes) + ['\n'.join(content)]),
                            **self.kwargs)

class DisplayPanel(RenderHTML):
    """
    Accepts a list of items to be rendered within a panel.

    An optional title can be provided.
    The panel decoration can be specified as low, medium or high
    low implies a thin red line above the title (default)
    medium displays any title in inverse video (but NO red line if no title)
    high displays the entire block in inverse video
    """

    def __init__(self, appearance="low", title=None):
        self.appearance = appearance
        self.title = title
        self.items = []

    def append(self, *items):
        self.items += items
        return self

    def __html__(self):
        content = []
        for item in self.items:
            content.append(self.item_render(item))
        content = HTMLDivClass("panel-body", '\n'.join(content))

        if self.title:
            content = HTMLDivClass("panel-heading",
                                   self.item_render(self.title)) + content
        return HTMLDivClass("panel panel-uoe-%s" % self.appearance, content)

class DisplayHeading(RenderHTML):

    def __init__(self, title=None, level="h2"):
        self.title = title or ''
        self.level = level

    def __html__(self):
        return HTMLString("<%s>%s</%s>" % (
            self.level, self.item_render(self.title), self.level))

class DisplayList(RenderHTML):
    """Make a list of the appended items.

    List type can be "unstyled" (default), "inline",
    or "ol" or "ul" for usual HTML behaviour.
    """

    def __init__(self, appearance="unstyled"):
        self.appearance = appearance
        self.listtype = 'ol' if appearance == 'ol' else 'ul'
        self.items = []

    def append(self, *items):
        self.items += items
        return self

    def __html__(self):
        content = ['<%s class="list-%s">' % (self.listtype, self.appearance)]
        for item in self.items:
            content.append('<li>' + self.item_render(item) + '</li>')
        content.append('</%s>' % self.listtype)
        return HTMLString('\n'.join(content))

class DisplayTable(RenderHTML):
    """
    Makes a table of entries ready for rendering.

    Optionally can be given a headings row and/or header column.
    Additional rows can be added until rendering.

    headings is the optional top row of headings
    row_headings is a boolean - True implies each row starts with a heading
    align is an optional list of column alignments
       the values are used in a cycle as required
       values can be: 'left', 'right', 'center', justify'
    style is table style from: 'striped', 'bordered', 'hover'
    ragged suppresses the default automatic row padding
    """

    def __init__(self, headings=[], row_headings=False,
                 align=None, style=None, ragged=False):
        self.headings = headings
        self.row_headings = row_headings
        self.align = align or ['auto']
        self.style = style
        self.rows = []
        self.ragged = ragged

    def append(self, *rows):
        self.rows += rows
        return self

    def __html__(self, class_=None, with_table_tag=True):
        if self.headings:
            align = itertools.cycle(self.align)
            rows = [(''.join(
                '<th style="text-align:%s">%s</th>' %
                (aln, self.item_render(col))
                for col, aln in zip(self.headings, align)))]
        else:
            rows = []

        if self.ragged:
            body_rows = self.rows
        else:
            body_rows = zip(*itertools.zip_longest(*self.rows, fillvalue=''))

        if self.row_headings:
            for row in body_rows:
                align = itertools.cycle(self.align)
                rows.append('<th style="text-align:%s">%s</th>' %
                            (align.__next__(), self.item_render(row[0])) +
                            ''.join('<td style="text-align:%s">%s</td>' %
                                    (aln, self.item_render(col))
                                    for col, aln in zip(row[1:], align)))
        else:
            for row in body_rows:
                rows.append(''.join(
                    '<td style="text-align:%s">%s</td>' %
                    (aln, self.item_render(col)) for col, aln in
                    zip(row, itertools.cycle(self.align))))

        table = '<tr>' + '</tr>\n<tr>'.join(rows) + '</tr>\n'

        if with_table_tag:
            if not class_:
                class_ = 'table-%s' % self.style if self.style else ''
            start = '<table class="table table-condensed %s table-responsive">\n' % class_
            end = '\n</table>'
            table = start + table + end

        return HTMLString(table)



class Select2Field(wtforms.SelectField):
    """
    A SelectField with support for JQuery select2

    Allows for a function to be provided with a callback to set choices.
    Any such function should return a sequence of (value, label) pairs.
    The rendered field will also have additional functionality
    provided by the Select2 plugin for jQuery.

    choices can be a callable (i.e. has a __call__ method) in which case
    it is called just before the form is rendered.

    If the underlying data is not text, coerce can be supplied, which will
    be called to convert values - often simply a type - e.g. int

    Handling of empty or null values is trickier than might be expected.
    Some widgets return the string "None", but will not accept it as a value.
    Some experimentation may be needed to find suitable values for
    none_text, none_value and empty_value

    If  multiple  is True a Select2MultipleField instance is returned.

    At instantiation various parameters can be set:
        coerce - callable to coerce non-string values (often a type)
        allow_new - if True, new values can be typed in
        nullable - if True, the value can be cleared
            (The  nullable  property also controls the value being None)
        none_text - string to display when the value is None
        none_value - input value to interpret as None (default: "None")
        empty_value - string to supply when field is empty (default '')
        auto_copy - if True, the key is automatically copied in the browser
    """

    def __new__(cls, *args, multiple=False, **kwargs):
        if multiple:
            return Select2MultipleField(*args, **kwargs)
        return super().__new__(cls, *args, **kwargs)

    def coerce_not_null(self, coerce_function):
        def coercer(value):
            try:
                return coerce_function(value)
            except:
                return None
        return coercer

    def __init__(self,
                 multiple=False,
                 allow_new=False, nullable=True, coerce=str,
                 none_text='(None)', none_value='None', empty_value='',
                 ajax=None, width='100%', auto_copy=None,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ajax = ajax
        self.allow_new = allow_new
        self.nullable = nullable
        self.coerce = self.coerce_not_null(coerce)
        self.none_text = none_text
        self.none_value = none_value
        self.empty_value = empty_value
        self.width = width
        self.auto_copy = auto_copy

    def _call_choices(self, value):
        if hasattr(self.choices, '__call__'):
            if not self.ajax or value in [self.empty_value, self.none_value]:
                value = None
            try:
                self.choices = self.choices(value)
            except:
                self.choices = self.choices()
        if hasattr(self.choices, '__iter__'):
            for choice in self.choices:
                if choice[0] in [None, self.empty_value, self.none_value]:
                    self.choices.remove(choice)
        else:
            self.choices = []


    def iter_choices(self):
        current = self.data
        self._call_choices(current)
        current_none = current in [None, self.empty_value, self.none_value]
        if self.ajax and current_none:
            yield (self.empty_value, self.none_text, False)
        else:
            if self.nullable or current_none:
                self.choices.insert(0, (self.empty_value, self.none_text))
            if self.allow_new and not current_none:
                if current not in [choice[0] for choice in self.choices]:
                    self.choices.insert(0, (current, current))
            for choice in super().iter_choices():
                yield choice


    def __call__(self, **kwargs):
        if 'style' not in kwargs:
            width = "min-width:%s;max-width:%s;width:%s" % (3 * (self.width,))
            kwargs['style'] = width
        if self.auto_copy:
            kwargs['geb_copy'] = True
        options = {'theme': "bootstrap"}
        if self.none_text:
            options['placeholder'] = "%s" % self.none_text
        if self.allow_new:
            options['tags'] = True
        if self.nullable:
            options['allowClear'] = True
        else:
            options['allowClear'] = False
            options['selectOnClose'] = True
        if self.ajax:
            options['ajax'] = {'sortResults': True,
                               'url': "select/%s/%s" % (self.ajax, self.id)}
        kwargs['geb_select2'] = json.dumps(options)
        return super().__call__(**kwargs)

    def pre_validate(self, form):
        self._call_choices(self.data)
        if self.data in [None, self.empty_value, self.none_value]:
            if not self.nullable:
                raise wtforms.validators.StopValidation(
                    self.gettext('Choose a value'))
        elif not self.allow_new and self.data:
            values = set(self.coerce(c) for c, _ in self.choices)
            if self.data not in values:
                raise ValueError(self.gettext('Not a valid choice'))


class Select2MultipleField(wtforms.SelectMultipleField):
    """
    A SelectField with support for JQuery select2

    Allows for a function to be provided with a callback to set choices.
    Any such function should return a sequence of (value, label) pairs.
    The rendered field will also have additional functionality
    provided by the Select2 plugin for jQuery.

    choices can be a callable (i.e. has a __call__ method) in which case
    it is called just before the form is rendered.

    If the underlying data is not text, coerce can be supplied, which will
    be called to convert values - often simply a type - e.g. int

    Handling of empty or null values is trickier than might be expected.
    Some widgets return the string "None", but will not accept it as a value.
    Some experimentation may be needed to find suitable values for
    none_text, none_value and empty_value

    If  multiple  is False a Select2Field instance is returned.

    At instantiation  various parameters can be set:
        coerce - callable to coerce non-string values (often a type)
        nullable - defaults True, if False at least one item must be selected
        allow_new - if True, new values can be typed in
        none_text - string to display when the value is None
        none_value - input value to interpret as None (default: "None")
        empty_value - string to supply when field is empty (default '')
        auto_copy - if True, the key is automatically copied in the browser
    """

    def __new__(cls, *args, multiple=True, **kwargs):
        if not multiple:
            return Select2Field(*args, **kwargs)
        return super().__new__(cls, *args, **kwargs)

    def __init__(self, *args,
                 multiple=True,
                 coerce=str, allow_new=False, nullable=True,
                 none_text='(None)', none_value='None', empty_value='',
                 ajax=None, width='100%', auto_copy=None,
                 **kwargs):
        super().__init__(*args, coerce=coerce, **kwargs)
        self.allow_new = allow_new
        self.nullable = nullable
        self.none_text = none_text
        self.none_value = none_value
        self.empty_value = empty_value
        self.ajax = ajax
        self.width = width
        self.coerce = coerce or str
        self.auto_copy = auto_copy

    def _call_choices(self):
        if hasattr(self.choices, '__call__'):
            self.choices = self.choices()
        if hasattr(self.choices, '__iter__'):
            for choice in self.choices:
                if choice[0] in [None, self.empty_value, self.none_value]:
                    self.choices.remove(choice)
        else:
            self.choices = []

    def iter_choices(self):
        self._call_choices()
        if self.ajax:
            if hasattr(self.choices, '__iter__'):
                dictchoice = {self.coerce(c[0]): c[1] for c in self.choices}
            else:
                dictchoice = {}
            choicelist = {}
            for value in self.data or []:
                if value in dictchoice:
                    choicelist[value] = dictchoice[value]
                elif self.allow_new:
                    choicelist[value] = value

            choices = (kv + (True,) for kv in choicelist.items())
            choices = sorted(choices, key=lambda choice: choice[1])
        else:
            if self.allow_new:
                for current in self.data or []:
                    if current not in [choice[0] for choice in self.choices]:
                        self.choices.insert(0, (current, current))

            choices = super().iter_choices()
        if self.coerce != str:
            for choice in choices:
                yield (repr(choice[0]), choice[1], choice[2])
        else:
            for choice in choices:
                yield choice

    def __call__(self, **kwargs):
        if 'style' not in kwargs:
            width = "min-width:%s;max-width:%s;width:%s" % (3 * (self.width,))
            kwargs['style'] = width
        if self.auto_copy:
            kwargs['geb_copy'] = True
        options = {'theme': "bootstrap"}
        if self.none_text:
            options['placeholder'] = self.none_text
        if self.allow_new:
            options['tags'] = True
        options['multiple'] = True
        if self.ajax:
            options['ajax'] = {'sortResults': True,
                               'url': "select/%s/%s" % (self.ajax, self.id)}
        kwargs['geb_select2'] = json.dumps(options)
        return super().__call__(**kwargs)

    def pre_validate(self, form):
        self._call_choices()
        if not self.data:
            if not self.nullable:
                raise ValueError(self.gettext("Choose at least one item"))
        elif not self.allow_new:
            values = set(self.coerce(c) for c, _ in self.choices)
            for d in set(self.data) - values:
                raise ValueError(self.gettext(
                    "'%(value)s' is not a valid choice for this field")
                                 % dict(value=d))

class SortableField(wtforms.SelectMultipleField):
    """A multiple-select where the only selection activity is ordering.

    The choices can be specificied as a callable, but are only used to
    associate values with labels.
    """
    _dictchoices = {}

    def __init__(self, *args, numbered=False, **kwargs):
        super().__init__(*args, **kwargs)
        self.numbered = numbered

    def _call_choices(self):
        if hasattr(self.choices, '__call__'):
            self.choices = self.choices()
        if not hasattr(self.choices, '__iter__'):
            self.choices = []
        elif not self._dictchoices:
            self._dictchoices = dict(self.choices)

        if self.object_data:
            map_src = self.object_data
        else:
            map_src = [c for c, _ in self.choices]
        self._mapping = [(self.id + '_' + str(c), d)
                         for c, d in enumerate(map_src)]

    def iter_choices(self):
        self._call_choices()
        for choice in super().iter_choices():
            yield choice

    def __call__(self, **kwargs):
        self._call_choices()
        if self.numbered:
            block, element = 'ol', 'li'
        else:
            block, element = 'div', 'div'

        html = []
        html.append('<input hidden name="%s" id="%s" geb_model="%s" />'
                    % (self.id, self.id, self.id))
        html.append('<%s geb_sortable="%s">' % (block, self.id))
        mapping = {d: c for c, d in self._mapping}

        for data in self.data:
            html.append(
                '<%s %s>'
                    '<div class="input-group">'
                        '<div class="input-group-addon">'
                            '<div class="glyphicon glyphicon-sort"'
                                ' aria-hidden="true"></div>'
                        '</div>'
                        '<div class="form-control" style="height:auto;">%s</div>'
                    '</div>'
                '</%s>' %
                (element,
                 wtforms.widgets.html_params(data_id=mapping.get(data)),
                 self._dictchoices.get(data, "ERROR"),
                 element))
        html.append('</%s>' % block)
        return HTMLString('\n'.join(html))

    def pre_validate(self, form):
        self._call_choices()
        for d in self.data:
            if d not in self._dictchoices:
                raise wtforms.validators.ValidationError(
                    self.gettext('Invalid choice in sortable list'))

    def process_formdata(self, valuelist):
        self._call_choices()
        if valuelist:
            mapping = dict(self._mapping)
            self.data = [mapping.get(d) for d in valuelist[0].split(',')]

    def process_data(self, valuelist):
        self._call_choices()
        if valuelist:
            self.data = valuelist
        else:
            self.data = [choice for choice, _ in self.choices if choice]


class ChoiceField(wtforms.SelectMultipleField):
    """
    A multiple-select, except displays a list of checkboxes.

    The choices can be specified as a callable.
    Iterating the field will produce subfields, allowing custom rendering of
    the enclosed checkbox fields.
    """
    widget = wtforms.widgets.ListWidget(prefix_label=False)
    option_widget = wtforms.widgets.CheckboxInput()

    def __init__(self, *args, nullable=False, **kwargs):
        super().__init__(*args, **kwargs)
        if hasattr(self.choices, '__call__'):
            self.choices = [choice for choice in self.choices()]


class FlagField(ChoiceField):
    """
    A choice field, except stores results as space separated list.

    This is used for Flags in the machines database.
    """
    widget = wtforms.widgets.ListWidget(prefix_label=False)
    option_widget = wtforms.widgets.CheckboxInput()

    def process_formdata(self, valuelist):
        if valuelist:
            self.data = ' ' + ' '.join(valuelist) + ' '
        else:
            self.data = ' '

    def pre_validate(self, form):
        if self.data:
            save = self.data
            self.data = self.data.split()
            super().pre_validate(form)
            self.data = save

    def process_data(self, value):
        if value:
            value = value.split()
        super().process_data(value)

def confirm_kwargs(confirm):
    """Produces keyword args associated with prompts for a field.

    Accepts keyword arguments:
    id_ = id of element to protect - default: find previous item in page
    text = text displayed in box - default: "Are you sure?"
    title = title of box - default: "Confirm"
    ok = text for confirm button - default: "Confirm" (but see below)
    cancel = text for cancel button - default: "Cancel"
    on_valid = if True confirmation will happen after validation
    done = if True confirmation will be bypassed

    If the ok text is not set, the value of the field is used a default,
    Only when no value is available is the default used.

    Note that if id_ is given, other values can be modified by ajax.
    This applies for SubmitField and DeleteField
    E.g. Returning: {'confirm': 'field': {'ok': 'OK'}}
         sets the ok text of 'field' to 'OK'
    """

    if confirm is None:
        return {}
    if confirm is True:
        confirm = {}
    result = dict(
        geb_confirm='y',
        geb_confirm_title=confirm.get('title'),
        geb_confirm_ok=confirm.get('ok'),
        geb_confirm_cancel=confirm.get('cancel'),
        geb_confirm_text=confirm.get('text'),
        geb_confirm_done=confirm.get('done') and 'y',
        geb_confirm_on_valid=confirm.get('on_valid') and 'y',
    )
    return {k: v for (k, v) in result.items() if v is not None}
        
class SubmitField(wtforms.SubmitField):
    """A standard SubmitField which geb puts at the end of a form.

    Appearance can be changed, defaults to a standard submit button.
    Setting spaced to True puts space above the button.
    This has no effect if the form has form_spaced set to True.
    Confirmation can be requested with confirm
      True for defaults or a dictionary - see confirm_kwargs
    """

    def __init__(self, label=None, validators=None, confirm=None,
                 appearance="Submit", spaced=False, **kwargs):
        self.appearance = appearance
        self.spaced = spaced
        self.confirm = confirm
        super().__init__(label, validators, **kwargs)

    def __call__(self, **kwargs):
        args = confirm_kwargs(self.confirm)
        args.update(kwargs)
        return super().__call__(**args)

class ActionField(wtforms.SubmitField):
    """A submit field which notes an action request.

    After submission, if the button is pressed, its details are promoted
    to the containing form.
    """
    def __init__(self, action, label=None, validators=None,
                 appearance="Action", **kwargs):
        self._action = action
        self.appearance = appearance
        self._form = kwargs.get('_form')
        if label is None:
            label = action
        super().__init__(label, validators, **kwargs)

    def process_formdata(self, valuelist):
        if len(valuelist) == 1 and valuelist[0]:
            if self._form is not None:
                self._form._action = self._action


class DeleteField(wtforms.BooleanField):
    """A submit field to request deletion of the contents of a (sub)form.

    If the button is pressed the details of the object to delete is
    promoted to the containing form.  The default label is 'Delete'.
    """
    def __init__(self, label='Delete', validators=None, **kwargs):
        self._form = kwargs.get('_form')
        super().__init__(label, validators, **kwargs)

    def process_formdata(self, valuelist):
        if len(valuelist) == 1 and valuelist[0]:
            if self._form is not None:
                self._form._delete = None

    def __call__(self, **kwargs):
        kwargs['geb_change'] = 'delbtn'
        kwargs['style'] = 'geb-delete'
        return super().__call__(**kwargs)

class DeleteButton(SubmitField):
    """A submit button to request deletion of the contents of a (sub)form.

    If the button is pressed the details of the object to delete is
    promoted to the containing form.  The default label is 'Delete'.
    """
    def __init__(self, label='Delete', validators=None, confirm=None, **kwargs):
        self._form = kwargs.get('_form')
        kwargs.setdefault('appearance', 'Delete')
        if confirm is None:
            confirm = {'ok': 'Delete'}
        kwargs['confirm'] = confirm
        
        super().__init__(label, validators, **kwargs)

    def process_formdata(self, valuelist):
        if len(valuelist) == 1 and valuelist[0]:
            if self._form is not None:
                self._form._delete = None

    def __call__(self, **kwargs):
        kwargs['geb_change'] = 'delbtn'
        return super().__call__(**kwargs)

class IntegerField(wtforms.IntegerField):
    """An interger field that defaults to using the html5 widget."""
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('widget', wtforms.widgets.html5.NumberInput())
        super().__init__(*args, **kwargs)

class NoInput(wtforms.widgets.Input):
    """A widget that doesn't even generate an input element."""

    def __init__(self, *args, **kwargs):
        pass

    def __call__(self, field, **kwargs):
        value = kwargs.get('value', field._value())
        return HTMLString('<div>%s</div>' % value)

class ReadOnlyInput(wtforms.widgets.Input):
    """An input widget that is marked readonly and disabled."""

    input_type = 'text'

    def __call__(self, field, **kwargs):
        kwargs.setdefault('readonly', True)
        # Some html elements also need disabled attribute to achieve the
        # expected UI behaviour.
        kwargs.setdefault('disabled', True)
        return super().__call__(field, **kwargs)

class ReadOnlyField(wtforms.StringField):
    """A field that displays read-only data.

    The string displayed can be the raw value supplied, or it
    can be selected from supplied choices.

    The choices are a list of (value, display) tuples.
    Alternatively a function which returns such a list can be provided
    If the function takes an argument, it is called with the supplied value
    as its first argument, which it may use to return a filtered list.

    Both validate and populate_obj are deliberately disabled.
    """

    widget = ReadOnlyInput()

    def __init__(self, label=None, validators=None,
                 nullable=None, allow_new=None, format=None,
                 coerce=None, choices=None, **kwargs):
        self.coerce = coerce
        self.choices = choices
        super().__init__(label, validators, **kwargs)

    def process_data(self, value):
        if hasattr(self.choices, '__call__'):
            try:
                self.choices = self.choices(value)
            except:
                self.choices = self.choices()
        if hasattr(self.choices, '__iter__'):
            for choice in self.choices:
                if choice[0] == value:
                    self.data = choice[1]
                    return
        self.data = value

    def validate(self, form, extra_validators=()):
        return True

    def populate_obj(self, obj, name):
        pass

class NoInputField(ReadOnlyField):
    widget = NoInput()


class FormGenerator(wtforms_alchemy.generator.FormGenerator):
    """ A class to override the default FormGenerator.

    We are using this to replace the standard Select field with Select2.
    This allows additional items from the info field to be used with
    Select2, such as  nullable  and  allow_new
    """
    def get_field_class(self, column):
        column_type = super().get_field_class(column)
        if column_type == wtforms_components.SelectField:
            column_type = Select2Field
        elif column_type == wtforms_components.fields.html5.DateTimeField:
            column_type = DateTimeField
        return column_type

    def select_field_kwargs(self, column):
        kwargs = super().select_field_kwargs(column)
        kwargs['nullable'] = column.nullable
        if kwargs['coerce'] == wtforms_alchemy.utils.null_or_unicode:
            kwargs['coerce'] = str
        for key in ['ajax', 'width', 'allow_new', 'multiple', 'nullable',
                    'none_text', 'none_value', 'empty_value', 'auto_copy']:
            if key in column.info:
                kwargs[key] = column.info[key]
        return kwargs


class ConfirmHiddenField(wtforms.fields.HiddenField):
    pass
    
class Form(wtforms.Form, GetSession, AppearanceClass):
    """A form that is aware of Add and Delete actions.

    Action buttons when pressed will update the form with their action
    data.  This can then be requested for use by the application.

    The form is also searched for a delete button, and its state found.
    populate_obj either adds or removes items from the database and session
    accordingly.  validate returns True when a delete is requested.

    _field_order is used to override field ordering (just) before __iter__
    Fields in the list are moved up the order just enough to ensure that
    the order of fields mentioned in _field_order is correct.
    Fields that are not mentioned are not re-ordered.

	form_spaced causes additional spacing to be inserted above all fields
 	in the form when rendered.

    A geb_confirm hidden field is added for handling confirmation.

    Override of __init__ is to ensure that _obj gets a copy of the obj
    argument if specified with or without keyword (wtforms_alchemy bug fix).
    Without this, the Unique validator clashes with itself.
    """

    geb_confirm = ConfirmHiddenField()

    def __init__(self, formdata=None, obj=None, **kwargs):
        kwargs['formdata'] = formdata
        kwargs['obj'] = obj
        super().__init__(**kwargs)
        self._obj = kwargs['obj']

    def get_action(self):
        action = getattr(self, '_action', None)
        return action

    def populate_obj(self, obj):

        session = self.get_session() if hasattr(self, 'get_session') else None

        if hasattr(self, '_delete'):
            if session and obj in session:
                session.delete(obj)
            else:
                self._delete = obj
        else:
            try:
                session.add(obj)
            except:
                pass
            super().populate_obj(obj)

        for flist in [x for x in self if isinstance(x, wtforms.FieldList)]:
            for form in [x for x in flist if isinstance(x, wtforms.FormField)]:
                delobj = getattr(form, '_delete', None)
                if delobj and session and delobj in session:
                    session.expunge(delobj)

    def validate(self):
        valid = hasattr(self, '_delete') or super().validate()
        if self.geb_confirm.data:
            if not valid:
                self.geb_confirm.data = ''
            return False
        return valid

    def append_validators(self, field_name, *validators):
        field = getattr(self, field_name)
        field.validators = getattr(field, 'validators', []) + list(validators)

    def __iter__(self):
        """Modifies _fields to respect _field_order.

        The instance variable of _field_order is copied for use during
        processing, and replaced with None on completion.
        (This avoids unintentionally modifying the class variable,
        and prevents us repeatedly re-ordering.)

        Any geb.SubmitField or geb.DeleteField  not mentioned in
        _field_order is moved to the end of the order.
        """

        field_order = getattr(self, '_field_order', [])
        if field_order is not None:
            field_order = list(field_order)
            fields = collections.OrderedDict(self._fields)
            self._fields = collections.OrderedDict()
            end_fields = collections.OrderedDict()
            while fields:
                (field, value) = fields.popitem(last=False)
                if (isinstance(value, (SubmitField, DeleteField)) and
                    field not in field_order):
                    end_fields[field] = value
                else:
                    while field in field_order:
                        first = field_order.pop(0)
                        if first in fields:
                            self._fields[first] = fields[first]
                            del(fields[first])
                    self._fields[field] = value
            for field, value in end_fields.items():
                self._fields[field] = value
            self._field_order = None
        return super().__iter__()

    def __html__(self, style=None, label=True):
        """Renders the form in the given style.

        Iterates over the fields in the form and creates suitable output
        for substitution in a template.
        """

        parts = [self.render_field(field, "col-md-9", "col-md-3")
                 for field in self]
        return HTMLString(
            '<form method="POST" class="form" novalidate action="" '
            'enctype="multipart/form-data" name="geb_form">\n'
            '<input type="submit" style="display: none" />' +
            '\n'.join(parts) +
            '\n</form>\n')

    def render_sub_labels(self, columns=None):
        """ Renders the labels for a sub-form as an inline block.

        Expects a list of CSS classes to be used for each column.
        """
        if not columns:
            columns = getattr(self, "_columns", itertools.cycle(['col-xs-3']))

        fields = [field for field in self if type(field) != ConfirmHiddenField]
        labels = []
        for field, class_ in zip(fields, columns):
            if field.type in ('SubmitField', 'ActionField'):
                labels.append(HTMLDivClass(class_, ''))
            else:
                labels.append(HTMLDivClass(class_, field.label()))
        return HTMLDivClass("row", '\n'.join(labels))

    def render_sub(self, columns=None):
        """ Renders a sub-form as an inline block.

        Expects a list of CSS classes to be used for each column.
        """

        if not columns:
            columns = getattr(self, "_columns", itertools.cycle(['col-xs-3']))

        fields = [field for field in self if type(field) != ConfirmHiddenField]
        parts = [self.render_field(field, class_)
                 for field, class_ in zip(fields, columns)]
        return HTMLDivClass("row", '\n'.join(parts))

    def field_with_model(self, field, class_=None, **attribs):
        geb_model = field.id.replace('-', '_')
        attribs['class_'] = class_
        attribs['geb_model'] = geb_model

        if isinstance(field, wtforms.BooleanField):
            attribs['geb_true_value'] = "y"
            attribs['geb_false_value'] = ""
        elif hasattr(field, 'checked'):
            attribs['geb_true_value'] = field.data
            attribs['geb_false_value'] = ""
        return field(**attribs)

    def render_field(self, field, class_=None, label_class=None):
        if field.description:
            description = HTMLDivClass(class_, "help-block", field.description)
            if label_class:
                description = HTMLDivClass(label_class, '') + description
        else:
            description = ''

        if field.type == 'FormField':
            return HTMLDivClass("", '\n'.join(
                [self.render_field(subfield, class_=class_,
                                  label_class=label_class)
                 for subfield in field]) + description, hide=field.id)
        elif isinstance(field, wtforms.FieldList):
            result = []
            for err in field.errors or []:
                if not hasattr(err, 'keys'):
                    result.append(HTMLDivClass(
                        "form-group has-error has-feedback",
                        "control-label has-error", err))
            need_labels = True
            for form in field:
                if need_labels:
                    labels = form.render_sub_labels()
                    need_labels = False
                    result.append(labels)
                result.append(form.render_sub())
            if hasattr(field, "add_button"):
                result.append(field.render_add_button(need_labels,
                                                      class_, label_class))
            return HTMLDivClass("",
             '\n'.join(result) + description, hide=field.id)
        elif isinstance(field, wtforms.fields.HiddenField):
            return self.field_with_model(field) + description
        else:
            parts = []
            row_cls = ["row"] if label_class else []
            if (getattr(self, "form_spaced", False) 
                or getattr(field, "spaced", False)):
                row_cls.append("geb-margin-top") 

            if field.errors:
                row_cls.append("form-group has-error has-feedback")
            if isinstance(field, wtforms.fields.SubmitField):
                if label_class:
                    parts.append(HTMLDivClass(label_class, ''))
                appearance = getattr(field, 'appearance', 'Submit')
                appearance = self.appearance_class(appearance)
                parts.append(HTMLDivClass(class_,
                   self.field_with_model(field, class_=appearance)))
            else:
                part2 = []
                for err in field.errors:
                    part2.append(HTMLDivClass("control-label has-error", err))
                if field.type in ('FlagField', 'ChoiceField', 'RadioField'):
                    part2.append(HTMLDivClass(
                        "form-inline",
                        ''.join([HTMLDivClass(
                            "input-group",
                            HTMLDivClass("input-group-addon", sub.label) +
                            HTMLDivClass("form-control",
                                         self.field_with_model(sub),
                                         style="width:auto")
                        ) for sub in field])))
                else:
                    part2.append(
                        self.field_with_model(field, class_='form-control'))
                    if field.errors:
                        part2.append(GlyphIcon(
                            "remove", "form-control-feedback"))
                if label_class:
                    parts.append(HTMLDivClass(label_class, field.label()))
                parts.append(HTMLDivClass(class_, '\n'.join(part2)))

            return HTMLDivClass(
                *(row_cls + ['\n'.join(parts) + description]),
                hide=field.id)

ModelForm = wtforms_alchemy.model_form_factory(
    Form, form_generator=FormGenerator)

@sqlalchemy.ext.declarative.as_declarative()
class Base(GetSession):
    @sqlalchemy.ext.declarative.declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    @classmethod
    def create_all(cls):
        cls.metadata.create_all(cls.metadata.bind)

    @classmethod
    def __fullname__(cls, column=None):
        """Returns a string to describe this table or column.

        This can be used thus when creating a
            relationship(..., secondary=Table.__fullname__, ...)
        When a schema is specified, SQLAlchemy sometimes gets confused
        about foreign keys etc.  Using the string including the schema
        seems to work around the problem.  This allows it to be done
        while still only referencing the python object.
        As a convenience, will also append a column name if supplied.
        """
        try:
            parts = [cls.__table_args__['schema']]
        except:
            parts = []
        parts.append(cls.__tablename__)
        if column:
            parts.append(column)
        
        return '.'.join(parts)

    @classmethod
    def get(cls, *args):
        return DBSession.query(cls).get(*args)

    @classmethod
    def by_id(cls, id):
        return DBSession.query(cls).filter_by(id=id).one_or_none()

    @classmethod
    def by_name(cls, name):
        return DBSession.query(cls).filter_by(name=name).one_or_none()

    @classmethod
    def filter_by(cls, **kwargs):
        return DBSession.query(cls).filter_by(**kwargs)

    @classmethod
    def none_to_many(cls, many='many', values=None, filters=None):
        none_to = DBSession.query(sqlalchemy.literal(None))
        if values is None:
            if filters:
                values = cls.filter_by(**filters).all()
            else:
                values = cls.get_all()
        setattr(none_to, many, values)
        return none_to

    @classmethod
    def coerce(cls, value):
        if isinstance(value, cls):
            return value
        try:
            if value.startswith(cls.__name__ + '(') and value.endswith(')'):
                value = value[len(cls.__name__) + 1: -1]
            return cls.get(json.loads(value))
        except:
            raise ValueError('Cannot convert %s' % value)

    def __repr__(self):
        return "%s(%s)" % (
            self.__class__.__name__,
                json.dumps(self.__mapper__.primary_key_from_instance(self)))

    def _asdict(self):
        items = (getattr(q, 'key', getattr(q, '__name__', None))
                 for q in sqlalchemy.inspect(self).mapper.all_orm_descriptors)
        return {item: getattr(self, item) for item in items if item}

    @classmethod
    def get_all(cls):
        return DBSession.query(cls).all()

class ImageField(wtforms.FileField):
    """A field to upload an Image from a file.

    This field reads in a file from the user, which must be an image.
    An invalid file will result in a validation error.
    A valid file will be internally re-rendered as a list of pairs of
    widths and thumbnails of that width.
    If no list of widths is provided, a standard set is used.
    The actual width of the image is also added to the set.
    If coerce is supplied, it is applied to each item in the list
    It is called thus: coerce(width=..., image=...)

    A thumbnail of any current image is also displayed.
    """
    def __init__(self,  label=None, validators=None,
                 coerce=None, widths=None, **kwargs):
        self.coerce = coerce    
        self.widths = set(widths or [1200, 992, 768])
        super().__init__(label, validators, **kwargs)

    def __call__(self, *args, **kwargs):
        main_field = super().__call__(*args, **kwargs)
        if not self.object_data:
            return main_field
        image = sorted(self.object_data, key=lambda i:i.width)[0]
        return ''.join((HTMLDivClass("col-xs-8", main_field),
                        HTMLDivClass("col-xs-4",
                                     '<img src="data:image/jpg;base64,%s" />'
                                     % base64.b64encode(image.image).decode())
        ))

    @staticmethod
    def make_image_set(image_file, widths=None):
        """Make a set of images of required widths.

        Returns a list of (width, image) tuples.
        Typically a coerce function is used to create a suitable object.
        """

        image = Image.open(image_file)
        if not widths:
            widths = []
        widths.add(image.size[0])
        if image.mode not in ("RGB", "RGBA"):
            image = image.convert("RGBA")
        result = []
        for width in widths:
            duplicate = image.copy()
            duplicate.thumbnail((width, image.size[1]))
            image_buffer = io.BytesIO()
            duplicate.save(image_buffer, 'jpeg', progressive=True)
            result.append((width, image_buffer.getvalue()))
        return result
                       
    def process_formdata(self, valuelist):
        if not self.data:
            self.data = []
        try:
            image_file = valuelist[0].file
        except:
            return

        try:
            self.data = self.make_image_set(image_file, self.widths)
            if self.coerce:
                self.data = [self.coerce(width=width, image=image)
                             for (width, image) in self.data]
        except:
            self.process_errors.append(
                self.gettext('Unable to process this image.'))

class DisplayImageSet:
    """Provides suitable html to display an image set.

    This provides a block of html which defines a picture element
    that tries to use the smallest available image which is larger
    than the current screen width.
    The  img  tag is used by browsers that do not support the
    picture tag.  Its CSS, figure and caption are always used.
    Images can have alt text, title and caption.
    An alt text value of "-" is replaced with "".
    """

    base = (
    "<picture>"
      "<source srcset='{url}0' media='only screen and (min-width: 1200px)' />"
      "<source srcset='{url}1200' media='only screen and (min-width: 992px)' />"
      "<source srcset='{url}992' media='only screen and (min-width: 768px)' />"
      "<source srcset='{url}768' media='only screen and (min-width: 0px)' />"
      "<figure>"
      "<img class='img-responsive' src='{url}0' alt='{alt}' title='{title}' />"
      "<figcaption>{caption}</figcaption>"
      "</figure>"
    "</picture>"
    )

    def __init__(self, url, alt="", title="", caption=""):
        self.url = url
        self.alt = alt if alt != '-' else ""
        self.title = title
        self.caption = caption

    def __str__(self):
        return self.base.format(url=self.url, alt=self.alt,
                                title=self.title, caption=self.caption)

    def __html__(self):
        return HTMLString(self.__str__())

    @staticmethod
    def best_image_at_width(imageset, width=None):
        """Returns the best image from the set based on width.

        This is a convenience function for use in views which provide
        individual images to be displayed as part of an image set.
        An imageset is a list of image object each of which have the 
        attributes image and width.
        The best image is the narrowest one whose width is greater than
        requested.  If no such image is available, or if requested width
        is 0, the widest image is returned.
        """
        images = sorted(imageset, key=lambda i: i.width)
        image = images[-1].image
        if width:
            for item in images:
                if item.width > width:
                    return item.image
        return image

class DateField(wtforms.DateField):
    """A field to collect date values.

    The default widget for this field is html5 aware.
    """

    def __init__(self, label=None, validators=None,
                 format='%Y-%m-%d', **kwargs):
        if not kwargs.get('widget'):
            kwargs['widget'] = wtforms.widgets.html5.DateInput()
        kwargs['format'] = format
        super().__init__(label, validators, **kwargs)

class TimeField(wtforms.DateTimeField):
    """A field to collect time values.

    The default widget for this field is html5 aware.
    Special care is needed with midnight as it has a boolean value of False.
    """

    def __init__(self, label=None, validators=None, format='%H:%M', **kwargs):
        if not kwargs.get('widget'):
            kwargs['widget'] = wtforms.widgets.html5.DateInput()
        kwargs['format'] = format
        super().__init__(label, validators, **kwargs)

    def process_formdata(self, valuelist):
        if valuelist:
            time_str = ' '.join(valuelist)
            try:
                self.data = datetime.datetime.strptime(
                    time_str, self.format).time()
            except ValueError:
                self.data = None
                raise ValueError(self.gettext('Not a valid time value'))

    def _value(self):
        if self.raw_data:
            return ' '.join(self.raw_data)
        else:
            if self.data is not None:
                return self.data.strftime(self.format)
            else:
                return ''


class DateTimeField(wtforms.DateTimeField):
    """A field to collect date and time separately.

    The date and time elements are rendered and evaluated separately,
    but the combined result is treated as a single value.
    """

    def __init__(self, *args, format=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._prefix = kwargs.get('_prefix', '')
        self.date_widget = wtforms.widgets.html5.DateInput()
        self.time_widget = wtforms.widgets.html5.TimeInput()

    def widget(self, field, geb_model=None, **render_kw):
        geb_date_model = geb_model + '_date' if geb_model else None
        date = self.date_widget(self.date_field, geb_model=geb_date_model,
                                style="min-width:50%", **render_kw)
        geb_time_model = geb_model + '_time' if geb_model else None
        time = self.time_widget(self.time_field, geb_model=geb_time_model,
                                style="min-width:50%", step=60, **render_kw)
        return HTMLDivClass("form-inline", date + time)

    def process(self, formdata, data=None):

        self.process_errors = []

        if not data:
            try:
                data = self.default()
            except TypeError:
                data = self.default

        self.object_data = data
        self.date_field = DateField('Date', widget=self.date_widget)
        self.date_field = self.date_field.bind(
            form=None, name='%s-date' % self.short_name,
            id='%s-date' % self.id, prefix=self._prefix, _meta=self.meta)

        self.time_field = TimeField('Time', widget=self.time_widget)
        self.time_field = self.time_field.bind(
            form=None, name='%s-time' % self.short_name,
            id='%s-time' % self.id, prefix=self._prefix, _meta=self.meta)

        try:
            self.date_field.process(formdata, data.date() if data else None )
            self.process_errors += self.date_field.process_errors
        except ValueError as e:
            self.process_errors.append(e.args[0])

        try:
            self.time_field.process(formdata, data.time() if data else None)
            self.process_errors += self.time_field.process_errors
        except ValueError as e:
            self.process_errors.append(e.args[0])

        self.data = data
        if formdata:
            try:
                self.raw_data = [' '.join(self.date_field.raw_data +
                                         self.time_field.raw_data)]
                if self.date_field.data and self.time_field.data is not None:
                    self.data = datetime.datetime.combine(self.date_field.data,
                                                          self.time_field.data)
                elif self.date_field.data or self.time_field.data is not None:
                    if not self.process_errors:
                        self.process_errors.append("Specify both date and time")
                else:
                    self.raw_data = ''
                    self.data = None
            except ValueError as e:
                self.process_errors.append(e.args[0])
                self.raw_data = ''
                self.data = None

class GlyphIcon(HTMLDivClass):
    def __new__(cls, glyph, classes=''):
        return super().__new__(cls, "glyphicon glyphicon-%s %s" %
                               (glyph, classes), '')

class InstantSearchForm(Form):
    """A Form for doing a search and its results.

    Creates a Form with a search box providing instant feedback.
    If an option tick box is required, provide a label for it.

    To distinguish from other search boxes in the same page
    a prefix can be used (defaults to "search").
    It is also possible to provide an  ajax_name  to distinguish calls
    to ajax (defaults to the prefix).
    Using debounce delays the ajax call to wait until the user has 
    stopped typing for the stated number of milliseconds.  Default value
    is 250 - set to 0 to disable.

    A convenience function  result_display  to display results.
    """
    
    search = wtforms.StringField()
    option = wtforms.BooleanField()

    def __init__(self, prefix='search', option_label=None, ajax_name=None,
                 style='condensed', placeholder='', description=None,
                 debounce=None,
                 *args, **kwargs):
        super().__init__(*args, prefix=prefix, **kwargs)
        self.prefix = prefix
        self.ajax_name = ajax_name or prefix
        self.style = style
        self.placeholder = placeholder
        self.description = description
        self.debounce = 250 if debounce is None else debounce
        if option_label:
            self.option.label = wtforms.Label(self.option.id, option_label)
        else:
            self.option = None

    def __html__(self):
        search_name = self.prefix + '_search'
        option_name = self.prefix + '_option'

        search = HTMLDivClass("input-group",
            self.field_with_model(self.search,
                class_="form-control",
                autocomplete="off",
                geb_session=search_name,
                geb_change="search/%s" % self.ajax_name,
                geb_init="search/%s" % self.ajax_name,
                geb_debounce=self.debounce,
                placeholder=self.placeholder,
            ) +
            HTMLDivClass("input-group-addon", GlyphIcon("search")))

        if self.description:
            description = HTMLDivClass("help-block", self.description)
        else:
            description = ''

        if self.option:
            option = HTMLDivClass("input-group",
                HTMLDivClass("input-group-addon", self.option.label()) +
                HTMLDivClass("form-control",
                    self.field_with_model(self.option,
                        geb_session=option_name,
                        geb_change="search/%s" % self.ajax_name,
                        geb_init="search/%s" % self.ajax_name,
                        class_="list-inline",
                    ), style="width:auto")
            )
        else:
            option = ''
        return HTMLDivClass("form", search + option) + description

    def result_display(self):
        """Render the results from this search as a div.

        An empty div with a suitable id is created.
        It is presumed that the div will be rewritten by an ajax call.
        """
        return HTMLDivClass("", "", id_="%s_search_results" % self.prefix)


class InstantSearch:

    @staticmethod
    def find_prev_and_next(here, choices):
        """Convenience method to find previous and next in choices.

        Takes a list of choices which are tuples: (link, text)
        Returns a tuple (prev, next) which are the previous and next
        items in the list relative to  here.
        If no suitable item exists, ' ' is returned instead.
        """
        links = [c[0] for c in choices]
        try:
            prev = ' '
            this = links.index(here)
            if this > 0:
                prev = links[this - 1]
            nxt = links[this + 1]
        except:
            nxt = ' '
        return (prev, nxt)

    @classmethod
    def return_div(cls, request, div_function, *args, **kwargs):
        """Wrap a function to return search results as a div to rewrite.

        This is a convenience wrapper function for returning search
        results to be displayed in the browser.
        The div_function and optional args and kwargs are specified.

        The div_function will be called thus:
            div_function(request, search, option, *args, **kwargs)

        The search and option values are the users current choices.
        This function should return content suitable to be rendered
        in to the results div on the web page.

        The resulting div is returned wrapped as a suitable response
        to the ajax call so that the div is updated accordingly.
        """
        ajax = AjaxRequest(request)
        if ajax.item.endswith(('_search', '_option')):
            prefix = ajax.item[:-7]
            option = ajax.model.get(prefix + '_option')
            search = ajax.model.get(prefix + '_search') or ""
            div = div_function(request, search, option, *args, **kwargs)

            return {
                'div': {prefix + "_search_results": div},
                'test': {prefix + '_search': search,
                         prefix + '_option': option},
            }
       
            
    @classmethod
    def return_links(cls, request, model, item, search_function,
                     with_prev_and_next=True):
        """Convenience function to return links from a search.

        This defines a function suitable and passes it to return_div.
        The  search_function  is called as:
            search_function(request, search, option)
        It is presumed to return a list of tuples to pass to LinkWidget
        If  with_prev_and_next  is True  and there are results to show
        Previous and Next links are also displayed.
        """
        def div_links(request, search, option,
                  search_function, with_prev_and_next=True):
            
            choices = search_function(request, search, option)

            results = DisplayList()
            if with_prev_and_next and choices:
                (prev, nxt) = cls.find_prev_and_next(request.referer, choices)
                prevnext = DisplayTable()
                up = DivClass("glyphicon glyphicon-arrow-up").__html__()
                down = DivClass("glyphicon glyphicon-arrow-down").__html__()
                prevnext.append([LinkWidget(prev, 'Previous' + up),
                                 LinkWidget(nxt, 'Next' + down)])
                results.append(prevnext)
                
            for choice in choices:
                results.append(LinkWidget(*choice))
            return results

        return cls.return_div(request, div_links, search_function,
                              with_prev_and_next=with_prev_and_next)

    @classmethod
    def return_rows(cls, request, model, item, search_function, headings=[]):
        """Convenience function to return a table from a search.

        This defines a function suitable and passes it to return_div.
        The  search_function  is called as:
            search_function(request, search, option)
        It is presumed to return a list of tuples to add to a table.

        If headings are supplied as a list, they are used as is.
        If headings is True, then the first value provided by 
        iterating  search_function  is used as the headings.
        """
        def div_rows(request, search, option, search_function, headings=[]):

            choices = search_function(request, search, option)
            if headings is True and choices:
                choices = iter(choices)
                try:
                    headings = next(choices)
                except StopIteration:
                    headings = []
            results = DisplayTable(headings=headings)
            for choice in choices:
                results.append(choice)
            return results

        return cls.return_div(request, div_rows, search_function,
                              headings=headings)

    @staticmethod
    def prepend_prev_next(here, links):
        """Takes a list of links and prepends previous and next links.

        Assumes that the current location is known and passed in.
        This will usually be the referer of the ajax request."""

        urls = [l[0] for l in links]
        try:
            prev = ''
            this = urls.index(here)
            if this > 0:
                prev = urls[this - 1]
            nxt = urls[this + 1]
        except:
            nxt = ''
        return [(prev, '<--'), (nxt, '-->')] + links

class InstantSearchPanel(InstantSearchForm, RenderHTML):
    """Create a Panel with a search form and its results.

    A title can be set for the panel (default "Search"). Other arguments
    are the same as for  InstantSearchForm
    """

    def __init__(self, title='Search', *args, **kwargs):
        super().__init__(*args,  **kwargs)
        self.title = title

    def __html__(self):
        if self.title:
            title_panel = HTMLDivClass("panel-heading",
                '<h2>%s</h2>' % self.item_render(self.title))
        else:
            title_panel = HTMLDivClass("panel-heading", '')

        return HTMLString(
            HTMLDivClass("panel",
                         "panel-uoe-medium" if self.title else "panel-uoe-low",
                         title_panel +
                         HTMLDivClass( "panel-body", super().__html__()),
                     ) +
            HTMLDivClass("panel panel-uoe-low", "panel-body",
                        self.result_display()))


class AjaxRequest:
    """
    Class to handle an ajax request from geb.pt and/or select2.

    Initialisation should be handed a request object which is
    expected to have a number of values set by the ajax call.

    ajax[0] - action: one of 'select', 'change', 'focus', etc.
    ajax[1] - name: the name specified in  ajax=  in the form model
    ajax[2] - item: the name or id of the form element
    term    - the value of the element to search on
    model   - current value of model, either in request or session
    """

    def __init__(self, request):
        ajax = request.matchdict.get('ajax', '')
        try:
            self.action = ajax[0]
        except IndexError:
            self.action = ''
        try:
            self.name = ajax[1]
        except IndexError:
            self.name = ''

        item = '/'.join(ajax[2:])
        if not item:
            try:
                item = request.json_body.get('item')
            except:
                pass
        self.item = item.replace('-', '_')
        self.term = request.params.get('term', '')

        try:
            self.model = request.json_body.get('geb', {})
            request.session['_geb_model'] = self.model
        except:
            self.model = request.session.get('_geb_model', {})

        if geb_bool(request, 'development') and self.action == 'fake':
            faking = self.item
            request.session[faking] = self.model.get(faking, '')

    def __repr__(self):
        return "<AJAX Request> action: %s, name: %s, item: %s, term: %s" % (
            self.action, self.name, self.item, self.term)

    @staticmethod
    def select_results(results, limit=500, limit_message='(Too many results)'):
        """A convenience method to return choices to select2.

        Drop down lists handled by select2 need the data as drop down
        lists handled by WTForms.  This takes the WTForms version and
        formats it for use by select2, up to a specified limit.
        """
        if len(results) > limit:
            if limit_message:
                return [{'id': None, 'text': limit_message}]
            else:
                return []
        return [{'id': choice[0], 'text': choice[1]} for choice in results]


def handle_cors(request, *cors_origins):
    """
    Modify response object to deal with CORS.

    Cross-origin resource sharing (CORS) is a mechanism that allows
    restricted resources on a web page to be requested from another
    domain outside the domain from which the first resource was served.

    This routine takes a list of domains to permit.
    The request is considered to match if the domain ends with one of
    the strings provided (e.g.  .ed.ac.uk  allows  www.geos.ed.ac.uk).

    The special case value '*' allows all domains.

    On match, request.response is updated, which does what is needed.
    """

    if '*' in cors_origins:
        request.response.headers.update({
            'Access-Control-Allow-Origin': '*',
        })
    else:
        origin = request.headers.get('Origin')
        if origin:
            for cors in cors_origins:
                if origin.endswith(cors):
                    request.response.headers.update({
                        'Access-Control-Allow-Origin': origin
                    })
                    return


class NavMenu:

    def __init__(self, request, *items, root=None):
        self.request = request
        if root:
            self.items = [(root, '/', None)] + list(items)
        else:
            self.items = list(items)

    def append(self, *items):
        self.items += list(items)
        return self

    def __html__(self):
        try:
            thisurl = self.request.current_route_url()
        except ValueError:
            thisurl = None

        trigger = '<h2 class="uoe-nav-trigger d-sm-none">Menu</h2>'
        menu = [trigger, '<ul class="uoe-nav">']
        for name, url, perm in self.items:
            try:
                url = self.request.route_url(url[0], **url[1])
            except:
                try:
                    url = self.request.route_url(url)
                except:
                    pass
            if perm is None or self.request.has_permission(perm):
                if url == thisurl:
                    class_ = 'uoe-nav-active'
                else:
                    class_ = ''
                menu.append('<li class="%s">'
                               '<a href="%s">%s</a>'
                            '</li>' % (class_, url, name))
        if geb_bool(self.request, 'development', 'menu_ease'):
            fake_ease = HTMLDivClass("input-group", 'EASE:'
                               '<input id="fake_ease" geb_session="fake_ease" '
                                     'geb_change="fake/fake_ease" '
                                     'geb_init="fake/fake_ease"></input>')
            menu.append('<li class="leaf active-trail">%s</li>' % fake_ease)
        menu.append('</ul>')
        return HTMLDivClass("col-sm-3 col-uoe-nav",
                            "region region-sidebar-first",
                            "block block-uoe-menu clearfix",
                            '\n'.join(menu))


def get_action(request, variable='action'):

    if request.method != 'POST':
        actions = request.session.pop_flash('action')
        if len(actions) > 0:
            return actions[-1]


class TextInput(wtforms.widgets.TextInput):
    """
    Render a single-line text input with an optional size"
    """

    def __init__(self, input_type=None, size=None):
        self.size = size
        super().__init__(input_type)

    def __call__(self, field, **kwargs):
        kwargs.setdefault('size', self.size)
        return super().__call__(field, **kwargs)


class TextArea(wtforms.widgets.TextArea):
    """
    Render a single-line text input with an optional size"
    """

    def __init__(self, rows=None, cols=None, ckeditor=False):
        self.rows = rows
        self.cols = cols
        self.ckeditor = ckeditor
        super().__init__()

    def __call__(self, field, **kwargs):
        kwargs.setdefault('rows', self.rows)
        kwargs.setdefault('cols', self.cols)
        if self.ckeditor:
            kwargs['class_'] = kwargs.get('class_', '') + ' ckeditor'
        return super().__call__(field, **kwargs)


class DifferentFrom:
    """
    Compares the values to other field(s), invalid if any the same.

    :param* fieldnames:
        The name(s) of the other field(s) to compare to.
    :param message:
        Error message to raise in case of a validation error. Can be
        interpolated with `%(other_label)s` and `%(other_name)s` to provide a
        more helpful error.
    """
    def __init__(self, *fieldnames, message=None):
        self.fieldnames = fieldnames
        self.message = message

    def __call__(self, form, field):
        if not (field.raw_data and field.raw_data[0]):
            return
        for fieldname in self.fieldnames:
            try:
                other = form[fieldname]
            except KeyError:
                raise wtforms.validators.ValidationError(
                    field.gettext("Invalid field name '%s'.") % fieldname)
            if field.data == other.data and field.short_name != fieldname:
                if not (other.raw_data and other.raw_data[0]):
                    continue
                d = {
                    'other_label': other.label.text if hasattr(other, 'label')
                                   else fieldname,
                    'other_name': fieldname
                    }
                message = self.message
                if message is None:
                    message = field.gettext(
                        'Must not be the same as %(other_label)s.')
                raise wtforms.validators.ValidationError(message % d)


class AllOrNone:
    """
    Compares the values of other fields, valid if ALL are None or not None

    :param* fieldnames:
        The name(s) of the other field(s) to compare to.
    :param message:
        Error message to raise in case of a validation error. Can be
        interpolated with `%(other_label)s` and `%(other_name)s` to provide a
        more helpful error.
    """
    def __init__(self, *fieldnames, message=None):
        self.fieldnames = fieldnames
        self.message = message

    def __call__(self, form, field):
        notnone = bool(field.raw_data and field.raw_data[0])
        for fieldname in self.fieldnames:
            try:
                other = form[fieldname]
            except KeyError:
                raise ValidationError(
                    field.gettext("Invalid field name '%s'.") % fieldname)
            othernone = bool(other.raw_data and other.raw_data[0])
            if notnone != othernone:
                d = {
                    'other_label': other.label.text if hasattr(other, 'label')
                                   else fieldname,
                    'other_name': fieldname
                    }
                message = self.message
                if message is None:
                    message = field.gettext(
                        'Must be set only when %(other_label)s is also set.')
                raise  wtforms.validators.ValidationError(message % d)


class ExactlyOne:
    """
    Compares the values of other fields, valid if exactly one is set.

    :param* fieldnames:
        The name(s) of the other field(s) to compare to.
    :param message:
        Error message to raise in case of a validation error. Can be
        interpolated with `%(other_label)s` and `%(other_name)s` to provide a
        more helpful error.
    """
    def __init__(self, *fieldnames, message=None):
        self.fieldnames = fieldnames
        self.message = message

    def __call__(self, form, field):
        notnone = bool(field.raw_data and field.raw_data[0])
        for fieldname in self.fieldnames:
            if field.short_name == fieldname:
                continue
            try:
                other = form[fieldname]
            except KeyError:
                raise ValidationError(
                    field.gettext("Invalid field name '%s'.") % fieldname)
            othernone = bool(other.raw_data and other.raw_data[0])
            if notnone == othernone:
                d = {
                    'other_label': other.label.text if hasattr(other, 'label')
                                   else fieldname,
                    'other_name': fieldname
                    }
                message = self.message
                if message is None:
                    message = field.gettext('Only one option must be set.')
                raise  wtforms.validators.StopValidation(message % d)


class JSONRenderer(pyramid.renderers.JSON):
    """A modified json renderer which can handle some additional
    object types."""

    def datetime_adapter(self, obj, request):
        return obj.isoformat()

    def str_adapter(self, obj, request):
        if hasattr(obj, '__html__'):
            return obj.__html__()
        return str(obj)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.add_adapter(datetime.datetime, self.datetime_adapter)
        self.add_adapter(datetime.date, self.datetime_adapter)
        self.add_adapter(object, self.str_adapter)

def geb_bool(request, *names):
    """Returns true if all the names are true in the settings.

    Checks the truthiness of the geb variables.
    """

    return all(pyramid.settings.asbool(
        request.registry.settings.get('geb.' + name)) for name in names)

def in_development(request):
    """Returns true if in the development environment.

    Checks the truthiness of the configuration setting  geb.development
    """
    return geb_bool(request, 'development')


class RequireEase:
    """Requires UUN in remote_user or redirects to login over https.

    Will raise a redirection if no remote_user value is available.
    In production over http this will redirect to https and insert /login
    at the beginning of the requested URL.
    Otherwise raise a 403 error (suggesting geb.fake_ease in development).
    """

    def __init__(self, function):
        self.function = function

    def __call__(self, request):
        if geb_bool(request, 'development'):
            fake_ease = request.session.get('geb.fake_ease', None)
            if fake_ease:
                print('DEBUG: Setting EASE to:', fake_ease)
                request.remote_user = fake_ease

        if request.remote_user:
            return self.function(request)

        if geb_bool(request, 'development'):
            return pyramid.httpexceptions.HTTPForbidden(
                'No EASE username - try geb.fake_ease in development.ini ?')

        return pyramid.httpexceptions.HTTPFound(
            "https://" + request.host + "/login" + request.path_qs)


class XMLRenderer:
    def __init__(self):
        self.adapters = {type(''): self._str_adapter}

    def __call__(self, info):
        def _render(obj, system):
            request = system.get('request')
            if request is not None:
                response = request.response
                response.content_type = 'application/xml'
            return self.render(obj, request, True)
        return _render

    def render(self, obj, request, decode=False):
        tree = self._convert(obj, request)
        xml_bytes = xml.etree.ElementTree.tostring(tree)
        if not decode:
            return xml_bytes
        return '<?xml version="1.0"?>' + xml_bytes.decode()

    @staticmethod
    def _str_adapter(obj, request):
        if obj is None:
            return ''
        return str(obj)

    def _is_list(self, obj):
        if not hasattr(obj, '__iter__') or hasattr(obj, 'keys'):
            return False

        for atype in self.adapters.keys():
            if isinstance(obj, atype):
                return False

        return True

    def _convert(self, obj, request, name=None, default_name='item'):
        thisname = name if name else getattr(obj, '__name__', default_name)

        child = xml.etree.ElementTree.Element(thisname)
        for atype in self.adapters.keys():
            if isinstance(obj, atype):
                child.text = self.adapters[atype](obj, request)
                break
        else:
            if hasattr(obj, '__iter__'):
                if hasattr(obj, 'keys'):
                    for key, val in obj.items():
                        if self._is_list(val):
                            for each in val:
                                child.append(self._convert(each, request, key))
                        else:
                            child.append(self._convert(val, request, key))
                else:
                    for item in obj:
                        child.append(self._convert(item, request, name, 'list'))
            elif hasattr(obj, 'isoformat'):
                child.text = obj.isoformat()
            elif obj is not None:
                child.text = str(obj)

        return child

    def add_adapter(self, adapter_type, adapter_func):
        self.adapters[adapter_type] = adapter_func

# https://docs.pyl
# onsproject.org/projects/pyramid-cookbook/en/latest/templates/customrenderers.html
class CSVRenderer(object):
    def __init__(self, info):
        pass

    def __call__(self, value, system):
        """ Returns a plain CSV-encoded string with content-type
        ``text/csv``. The content-type may be overridden by
        setting ``request.response.content_type``."""
        
        request = system.get('request')
        if request is not None:
            response = request.response
            ct = response.content_type
            if ct == response.default_content_type:
                response.content_type = 'text/csv'
                # Force UTF-8 BOM so it opens in Excel correctly
                response.charset = 'utf-8-sig'

        fout = io.StringIO()
        writer = csv.writer(fout, delimiter=',', quotechar='"',
                            quoting=csv.QUOTE_MINIMAL)

        writer.writerow(value.get('header', []))
        writer.writerows(value.get('rows', []))

        return fout.getvalue()


class FormField(wtforms.FormField):

    def __init__(self, form_class, label=None, validators=None, **kwargs):
        super().__init__(form_class, label, validators, **kwargs)
        try:
            self.form_default_class = self.form_class.Meta.model
        except:
            self.form_default_class = None

    def process(self, formdata, data=None):
        if formdata is None and data is None:
            try:
                data = self.default()
            except TypeError:
                data = self.default
            self._obj = data

        self.object_data = data

        prefix = self.name + self.separator
        if isinstance(data, dict):
            self.form = self.form_class(formdata=formdata, prefix=prefix, **data)
        else:
            self.form = self.form_class(formdata=formdata, obj=data, prefix=prefix)

    def populate_obj(self, obj, name):
        candidate = getattr(obj, name, None)
        if candidate is None:
            if self.default is None:
                self.default = self.form_default_class
            if self._obj is None:
                try:
                    data = self.default()
                except TypeError:
                    data = self.default
            self._obj = data
            if self._obj is None:
                raise TypeError('populate_obj: cannot find a value to populate from the provided obj or input data/defaults')
            candidate = self._obj
            setattr(obj, name, candidate)

        self.form.populate_obj(candidate)


class FieldList(wtforms.FieldList):
    """
    An ordered list of multiple instances of the same field.

    Like the upstream one, but with additional rendering features.
    Specifically it has an "add another one" button which is also tied
    to a div which it clones to do so.
    """

    def __init__(self, unbound_field, label=None, validators=None,
                 add_button=None, limit=None, **kwargs):
        self.add_button = add_button
        self.limit = limit
        super().__init__(unbound_field, label, validators, **kwargs)
        self.id = self.id.replace('_', '-')

    def ghost_entry(self):
        placeholder='%'
        name = '%s-%s' % (self.short_name, placeholder)
        id = '%s-%s' % (self.id, placeholder)
        field = self.unbound_field.bind(
            form=None, name=name, prefix=self._prefix, id=id, _meta=self.meta)
        field.process(None, self.default)
        return field

    def render_add_button(self, need_labels=False,
                          class_=None, label_class=None):
        if not self.add_button:
            return ''
        if self.limit is not None:
            if self.last_index + 1 >= self.limit:
                return ''
        attrs = {}
        if self.limit:
            attrs['geb_limit'] = self.limit
        attrs['geb_index'] = self.last_index
        attrs['geb_click'] = 'addbtn'
        attrs['geb_model'] = self.id
        attrs['id'] = self.id + '-button'
        attrs['type'] = 'button'

        attrs = ' '.join(('%s=%s' % attr for attr in attrs.items()))
        button = ('<button class="btn btn-default btn-sm" %s>%s'
                  '</button>' % (attrs, self.add_button))
        if class_:
            button = HTMLDivClass(class_, button)
        if label_class:
            button = HTMLDivClass(label_class, '') + button
        
        ghost = self.ghost_entry()
        if need_labels:
            labels = ghost.render_sub_labels()
            if label_class:
                labels = ('<script type="text/template" id="%s-labels">%s'
                          '</script>' % (self.id, labels))
        else:
            labels = ''

        rendered = ghost.render_sub()
        rendered = rendered.replace('<script>', '<geb-script>')
        rendered = rendered.replace('</script>', '</geb-script>')
        ghost = ('<script type="text/template" id="%s">%s</script>' %
                 (self.id, rendered))

        return HTMLDivClass("row", button + labels + ghost)
