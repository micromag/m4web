import pyramid.security
import sqlalchemy as sa
import wtforms
import decimal

from . import geb
from . import orm


class StringDecimal(sa.types.TypeDecorator):
    """ Class to convert numbers stored in the database as strings
        to numeric values
        https://stackoverflow.com/questions/29949970/convert-string-to-float-sqlalchemy/30004254#30004254
    """

    impl = sa.String

    def process_literal_param(self, value, dialect):
        return str(decimal.Decimal(value)) if value is not None else None

    process_bind_param = process_literal_param

    def process_result_value(self, value, dialect):
        return decimal.Decimal(value) if value is not None else None


class Model_(orm.Model):
    """ Inherit the 'Model' class from Les' definition. """

    pass

    @classmethod
    def search_parameters(
        cls,
        material_name,
        geometry_name,
        geometry_size1,
        geometry_size2,
        material_temperature1,
        material_temperature2,
        display_limit,
    ):
        """ Define search function for 'Model_'.
            Return a count of the number of matches
            and a set of models no greater in numbert than display_limit """
        query = (
            geb.DBSession.query(cls)
            .join(Geometry_)
            .join(orm.ModelMaterialAssociation)
            .join(Material_)
            .filter(Material_.name == material_name)
            .filter(Geometry_.name == geometry_name)
            .filter(Geometry_.size_decimal.between(
                geometry_size1, geometry_size2))
            .filter(
                Material_.temperature_decimal.between(
                    material_temperature1, material_temperature2
                )
            )
        )
        query = query.with_entities(
            cls.id,
            Geometry_.name.label("geometry_name"),
            Geometry_.size_decimal,
            Material_.name.label("material_name"),
            Material_.temperature_decimal,
            cls.mx_tot,
            cls.my_tot,
            cls.mz_tot,
            cls.h_tot,
            cls.e_tot,
        )
        row_count = query.count()
        if row_count > display_limit:
            return query.limit(display_limit).all(), row_count
        else:
            return query.all(), row_count
        return query.all(), row_count

    @classmethod
    def get_by_id(cls, id):
        """ Return 'Model_' object given id """
        query = (
            geb.DBSession.query(cls)
            .join(Geometry_)
            .join(orm.ModelMaterialAssociation)
            .join(Material_)
            .filter(cls.id == id)
        )
        query = query.with_entities(
            Geometry_.name.label("geometry_name"),
            Geometry_.size_decimal,
            Material_.name.label("material_name"),
            Material_.temperature_decimal,
            cls.id,
            cls.unique_id,
            cls.mx_tot,
            cls.my_tot,
            cls.mz_tot,
            cls.h_tot,
            cls.e_tot,
        )
        print(query.count())
        return query.one_or_none()


class Geometry_(orm.Geometry):
    """ Inherit the 'Geometry' class from Les' definition. """

    # Convert column size (a decimal stored as a string) to a Python decimal
    size = None
    size_decimal = sa.Column("size", StringDecimal)

    @classmethod
    def get_choices(cls):
        """ Return unique set of paired name values
            for use in dropdown select lists """
        query = geb.DBSession.query(cls.name, cls.name).distinct()
        query = query.order_by(cls.name)
        return query.all()


class Material_(orm.Material):
    """ Inherit the 'Material' class from Les' definition. """

    # Convert column temperature (a decimal stored as a string)
    # to a Python Decimal
    temperature = None
    temperature_decimal = sa.Column("temperature", StringDecimal)

    @classmethod
    def get_choices(cls):
        """ Return unique set of paired name values
            for use in dropdown select lists """
        query = geb.DBSession.query(cls.name, cls.name).distinct()
        query = query.order_by(cls.name)
        return query.all()


class ModelsSearchForm(geb.Form):
    """ A WTForms search form to select model runs/solutions
        based on input parameters. """

    # Override geb & superclass validate
    def validate(self):
        return False

    form_spaced = True

    # Form elements
    material_name = geb.Select2Field(
        label="Material name", none_text="(required)",)
    geometry_name = geb.Select2Field(
        label="Geometry name", none_text="(required)",)
    geometry_size_minimum = wtforms.fields.DecimalField(
        places=3, label="Geometry size minimum"
    )
    geometry_size_maximum = wtforms.fields.DecimalField(
        "Geometry size maximum")
    material_temperature_minimum = wtforms.fields.DecimalField(
        places=3, label="Material temperature minimum"
    )
    material_temperature_maximum = wtforms.fields.DecimalField(
        places=3, label="Material temperature maximum"
    )
    submit = geb.SubmitField("Show report", appearance="btn-uoe")
    reset = geb.SubmitField("Clear form", appearance="button medium")


class ApplicationRoles(geb.Base):
    """ Manage role permissions using GeoSciences Roles app """

    # 20200306 Not yet in use (CH)
    __tablename__ = "app_roles"
    __table_args__ = {"schema": "ops$apps_roles"}
    app_role_id = sa.Column(sa.Integer, primary_key=True)
    uun = sa.Column(sa.String)
    app_name = sa.Column(sa.String)
    role_name = sa.Column(sa.String)
    app = "m4web"

    @classmethod
    def get_roles(cls, uun, request=None):
        if geb.in_development(request):

            fake = request.session.get("geb.fake_ease", None)
            if fake:
                uun = fake
        try:
            result = (
                geb.DBSession.query(cls.role_name)
                .filter(cls.uun == uun, cls.app_name == cls.app)
                .all()
            )
            result = [row.role_name for row in result]
        except:
            result = []

        if geb.in_development(request):
            result.append("debug")

        return result


class Root:
    """ Configure app permissions """

    # 20200306 Not yet in use (CH)
    def __init__(self, request):
        self.__acl__ = [
            # (pyramid.security.Allow, 'debug',
            #     ('view_privileged', 'view_user',)),
            # (pyramid.security.Allow, 'debug', ('view_user',)),
            (pyramid.security.Allow, "debug", (
                "view", "edit",)),
            (pyramid.security.Allow, 'view', 'view'),
        ]

        self.__acl__.append(
            (
                pyramid.security.Deny,
                pyramid.security.Everyone,
                pyramid.security.ALL_PERMISSIONS,
            )
        )
